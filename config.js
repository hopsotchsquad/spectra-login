"use strict";

const fs = require("fs");

class Config {
	constructor() {
		this.client = "https://spectra.reachinghalcyon.com";
		this.saltRounds = 12;
		this.sysops = ["lights", "jd"];
		this.port = 8123;
		this.sim = {
			ip: "144.217.4.20",
			host: "spectra.reachinghalcyon.com",
			sPort: 8010,
			lPort: 8124
		};
		this.auth = {
			" ": 0, //No Auth
			"+": 0,
			"%": 0,
			"@": 1, //Limited Auth
			"&": 2, //Standard Auth
			"~": 3 //Administration Auth
		};
		this.actions = {
			vemail: 0,
			vphone: 0,
			cemail: 0,
			cphone: 0,
			recoverpass: 0,
			regdate: 0,
			seen: 0,
			verify: 1,
			deregister: 2,
			ban: 2,
			unban: 2,
			setauth: 2,
			disableuserid: 3,
			health: 3,
			eval: 3
		}

		//These ones are all you jd - I have the email account info I can pass you, though
		try {
			this.banned = fs.readFileSync("conf/banned.csv", "utf-8").split(",");
		} catch(e) {
			this.banned = [];
		}
		try {
			this.ssl = require("./conf/ssl.js");
		} catch(e) {
			this.ssl = null;
		}
		try {
			this.mysql = JSON.parse(fs.readFileSync("conf/mysql.json", "utf-8"));
		} catch(e) {
			this.mysql = null;
		}
		try {
			this.email = JSON.parse(fs.readFileSync("conf/email.json", "utf-8"));
		} catch(e) {
			this.email = null;
		}
		try {
			this.auth = JSON.parse(fs.readFileSync("conf/oauth.json", "utf-8"));
		} catch(e) {
			this.auth = null;
		}
	}
}

module.exports = Base => class extends Base {
	constructor() {
		super();
		this.conf = new Config();
	}
}