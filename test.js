'use strict';

global.ctx = this; //For Eval - Bad Practice, I know

global.toId = function(str) {
	return ('' + str).toLowerCase().replace(/[^a-z0-9]+/g, '');
}

let op = null;
for (let i = 0; i < process.argv.length; i++) {
	if (i < 2) continue;
	op = process.argv[i];
	break;
}

if (!op) {
	console.log("No Test Specified; Exiting...");
	process.exit();
} else {
	op = toId(op);
}

if (!require.resolve(`./test/${op}`)) {
	const options = require("fs").readdirSync("test");
	if (options && options.length > 0) {
		console.log("Invalid Test id: " + op);
		console.log("Valid Options: " + options.join(", "));
	} else {
		console.log("No Tests Available...");
	}
	process.exit();
}

const Mixin1 = require("./methods");
const Mixin2 = require("./parser");
const Mixin3 = require("./config");
class Core {};

class Methods extends Mixin1(Mixin2(Mixin3(Core))) {
	constructor() {
		super();
		this.dbInit(); //Initialize DB
	}
}

const script = require(`./test/${op}`);
script.start(new Methods());