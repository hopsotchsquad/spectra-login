"use strict";
class core {}
class C extends require("./config")(core) {constructor() {super();}}
const c = new C();
const Config = c.conf;
const protocol = Config.ssl ? require("https") : require("http");
const port = process.argv[2];
const tasks = new Map();
class Task extends require("events") {
	constructor(started, data, header) {
		super();
		this.id = genCode(16);
		this.data = data;
		this.header = header;
		this.then = null;
		this.started = started;
		this.timeout = setTimeout(() => {this.emit("destroy")}, 1000 * 60 * 5);
	}
}
function genCode(t) {
	const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	let str = "";
	while(str.length < t) {
		str += chars.charAt(Math.floor(Math.random() * chars.length));
	}
	return str;
}
function parse(data) {
	let obj = {};
	let members = data.split("||||");
	let member, value;
	for (const i of members) {
		member = i.split("////");
		if (member === "true") {
			value = true;
		} else if (member === "false") {
			value = false;
		} else value = member[1];
		obj[member[0]] = value;
	}
	return obj;
}
function listener(req, res) {
	req.on('data', (data) => {
		data = data.toString();
		if (data.charAt(0) === "{") {
			data = JSON.parse(data);
		} else data = parse(data);
		data.ip = req.connection.remoteAddress.replace(/::ffff:/, '');; //Attach IP to Request obj
		let task = new Task(Date.now(), data, {'Access-Control-Allow-Origin': '*'});
		task.then = (reply) => {
			res.writeHead(200, task.header);
			res.end(JSON.stringify(reply.data));
			task.emit("destroy");
		}
		task.on("destroy", () => {tasks.delete(task.id);});
		tasks.set(task.id, task);
		process.send({id: task.id, data: task.data});
	});
}
process.on("message", (reply) => {if (tasks.has(reply.id)) tasks.get(reply.id).then(reply);});
const server = protocol.createServer(...function(){return Config.ssl?[Config.ssl,listener]:[listener];}.call(this)).listen(parseInt(port), function() {console.log(`${Config.ssl ? "HTTPS" : "HTTP"} Server listening at localhost:${port}/`);});