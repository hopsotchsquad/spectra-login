"use strict";

const mysql = require('mysql');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');

module.exports = Base => class extends Base {
	constructor() {
		super();

		//Database
		this.dbUsers = "users";
		this.dbSessions = "sessions";
	}

	dbInit() {
		this.queryUsers(`CREATE TABLE IF NOT EXISTS ${this.dbUsers} (
			userid varbinary(255) NOT NULL,
			usernum int(11) NOT NULL AUTO_INCREMENT,
			username varbinary(255) NOT NULL,
			passwordhash varbinary(255) DEFAULT NULL,
			email varbinary(255) DEFAULT NULL,
			phone varbinary(255) DEFAULT NULL,
			verified int(11) NOT NULL DEFAULT '0',
			supporter int(11) NOT NULL DEFAULT '0',
			pendingemail varchar(255) DEFAULT NULL,
			pendingsms varchar(255) DEFAULT NULL,
			lastseen bigint(20) NOT NULL DEFAULT '0',
			registertime bigint(20) NOT NULL,
			auth varchar(255) NOT NULL DEFAULT ' ',
			banstate int(11) NOT NULL DEFAULT '0',
			ip varchar(255) NOT NULL DEFAULT '',
			avatar varbinary(255) DEFAULT NULL,
			loginip varbinary(255) DEFAULT NULL,
			PRIMARY KEY (userid),
			UNIQUE KEY usernum (usernum)
		) ENGINE=InnoDB AUTO_INCREMENT=7379773 DEFAULT CHARSET=utf8`, (err) => {
			if (err) {
				console.log("Error Initializing DB " + this.dbUsers + ":");
				console.log(err);
			}
		});

		this.queryUsers(`CREATE TABLE IF NOT EXISTS ${this.dbSessions} (
			session bigint(20) NOT NULL AUTO_INCREMENT,
			sid varchar(255) NOT NULL,
			userid varchar(255) NOT NULL,
			time int(11) NOT NULL,
			timeout int(11) NOT NULL,
			ip varchar(255) NOT NULL,
			PRIMARY KEY (session)
		) ENGINE=InnoDB AUTO_INCREMENT=1700523 DEFAULT CHARSET=utf8`, (err) => {
			if (err) {
				console.log("Error Initializing DB " + this.dbSessions + ":");
				console.log(err);
			}
		});
	}

	queryUsers(str, callback) {
		const connection = mysql.createConnection(this.conf.mysql);

		connection.on('error', (err) => {
			callback(err, null);
			return connection.destroy();
		});

		connection.connect((err) => {
			if (err) {
				callback(err, null);
				return connection.destroy();
			}
			connection.query(str, (err, rows) => {
				if (err) {
					callback(err, null);
					return connection.destroy();
				}
				return callback(null, rows);
			});
			connection.end();
		});
	}

	genCode(t) {
		const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		let str = "";
		while(str.length < t) {
			str += chars.charAt(Math.floor(Math.random() * chars.length));
		}
		return str;
	}

	genSID() {
		return this.genCode(18);
	}

	challenge(challstr, ip) {
		return {pass: true}; //For Debug
		return new Promise(async function(resolve, reject) {
			const data = await this.send([
				this.conf.sim.host,
				this.conf.sim.sPort
			], {
				act: "chall",
				challstr: challstr,
				ip: ip
			});
			resolve(data);
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	send(target, data) {
		if (!data) return null;
		return new Promise((resolve, reject) => {
			const req = require("https").request({
				host: this.conf.sim.host, //Temp hardcode this for debugging
				port: this.conf.sim.sPort,
				path: "/",
				method: "POST",
				agent: false,
				headers: {
					"Access-Control-Allow-Origin": "*"
				}
			}, (res) => {
				res.on("data", reply => {
					if (typeof reply === "string") reply = JSON.parse(reply);
					resolve(reply);
				});

				res.on("error", err => {
					reject(err)
				});
			});

			req.on("error", err => {
				reject(err)
			});
			req.write(JSON.stringify(data));
			req.end();
		}).catch((err) => {
			console.log(err);
		});
	}

	isRegistered(userid) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err) {
					console.log(err);
					resolve(false);
					return;
				}
				if (rows && rows[0]) {
					let timeout = parseInt(rows[0].registertime) + (1000 * 60 * 60 * 24 * 7);
					if (parseInt(rows[0].verified) <= 3 && Date.now() >= timeout) {
						this._deregister(userid);
						resolve(false);
					} else {
						resolve(true);
					}
				} else {
					resolve(false);
				}
			});
		});
	}

	validateEmail(email) {
		let pass = false;
		const valid = ["@gmail.com", "@hotmail.com", "@yahoo.com", "@protonmail.com", "@live.com", "@outlook.com"];
		for (const i of valid) {
			if (email.includes(i)) {
				pass = true;
				break;
			}
		}

		return pass;
	}

	validatePhone(phone) {
		phone = phone.replace(/[^0-9]+/g, '');
		if (isNaN(parseInt(phone))) return false;
		if (phone.length < 10) return false;
		return true;
	}

	_setAuth(userid, auth) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`UPDATE ${this.dbUsers} SET auth=${mysql.escape(auth)} WHERE userid=${mysql.escape(userid)}`, (err, results) => {
				if (err) return resolve(err);
				resolve(false);
			});
		});
	}

	_getAuth() {
		return new Promise((resolve, reject) => {
			const tasks = [];
			const users = [];
			const auth = ["+", "%", "@", "&", "~"];

			for (const i of auth) {
				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`SELECT username FROM users WHERE auth=${mysql.escape(i)}`, (err, rows) => {
						if (err) {
							console.log(err);
							return resolve([]);
						}
						if (!rows || !rows[0]) {
							return resolve([]);
						}
						let entries = [];
						for (const j of rows) {
							entries.push(j.username + "," + i);
						}
						resolve(entries);
					});
				}));
			}

			Promise.all(tasks).then((pr) => {
				for (const i of pr) {
					users.splice(0,0,...i);
				}

				resolve({status: 1, reply: users.join("|")});
			});
		});
	}

	_getEmail(userid) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT email FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, results) => {
				if (!err && results && results[0]) {
					let email = results[0].auth || null;
					resolve(email);
				} else {
					if (err) console.log(err);
					resolve(null);
				}
			});
		});
	}

	auth(userid) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT auth FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, results) => {
				if (!err && results && results[0]) {
					let auth = results[0].auth || " ";
					resolve(auth);
				} else {
					if (err) console.log(err);
					resolve(" ");
				}
			});
		});
	}

	can(userid, action, target) {
		return new Promise(async function(resolve, reject) {
			let userauth = await this.auth(userid);
			let targetauth = target ? await this.auth(target) : null;
			if (this.conf.auth[userauth] && //Valid Auth Level
				this.conf.auth[userauth] >= this.conf.actions[action] && //Meets minimum Auth requirement
				(!targetauth || ( //No Target Or
					this.conf.auth[targetauth] && //Target auth is valid
					this.conf.auth[targetauth] !== 3 && //Target isnt an admin
					this.conf.auth[targetauth] < this.conf.auth[userauth] //Targets auth is lower than users
				))
			) {
				resolve(true);
			} else {
				resolve(false);
			}
		}.bind(this));
	}

	canLink(type, value) {
		let max = 0;
		let supporter = null;
		return new Promise((resolve, reject) => {
			switch(type) {
			case "email":
				this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE email=${mysql.escape(value)}`, (err, results) => {
					if (err) {
						console.log(err);
						resolve(false);
					}

					if (!results || !results[0]) return resolve(true);
					if (results.length >= 50) return resolve(false);

					//Check to see if this user is a supporter
					supporter = [0, results.length];
					for (const i of results) {
						if (i.supporter == 1) supporter[0]++;
					}

					supporter = supporter[0] - supporter[1] === 0 ? true : false;

					max = supporter ? 50 : 5;

					if (results.length < max) {
						resolve(true);
					} else {
						resolve(false);
					}
				});
				break;
			case "phone":
				this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE phone=${mysql.escape(value)}`, (err, results) => {
					if (err) {
						console.log(err);
						resolve(false);
					}

					if (!results || !results[0]) return resolve(true);
					if (results.length >= 50) return resolve(false);

					//Check to see if this user is a supporter
					supporter = [0, results.length];
					for (const i of results) {
						if (i.supporter == 1) supporter[0]++;
					}

					supporter = supporter[0] - supporter[1] === 0 ? true : false;

					max = supporter ? 50 : 5;

					if (results.length < max) {
						resolve(true);
					} else {
						resolve(false);
					}
				});
				break;
			}
		});
	}

	verificationEmail(username, email, code) {
		const transport = nodemailer.createTransport({
			service: 'gmail',
			auth: this.conf.auth
		});
		const options = {
			to: `${email}`,
			subject: 'Verify your Email for Spectra!',
			text: `Please confirm the registration of '${username}' on ${this.conf.client} by visting the following link ${this.conf.client}/confirm.html?c=${code} within a week from the day you receive this email. This link will expire in 1 week. If this is a mistake, you can cancel the verification request here: ${this.conf.client}/cancel.html?c=${code}`
		}

		transport.sendMail(options, (err, info) => {
			if (err) {
				console.log("SendMail Error:")
				console.log(err);
			}
			this.send([
				this.conf.sim.host,
				this.conf.sim.sPort
			], {
				act: "evsent",
				userid: toId(username),
				email: email,
				success: err ? "0" : "1" 
			});
		});
	}

	verificationSMS(username, phone, code) {
		return;
		phone = phone.replace(/[^0-9]+/g, '');

		//We're just going to send to all carriers we can think of and hope one works
		const car = require("./carriers");
		const transporter = nodemailer.createTransport(this.conf.email);
		const message = {
			from: 'Spectra No-Reply',
			to: null,
			subject: 'Verify your Phone for Spectra!',
			text: `Please confirm the registration of your username '${username}' on Spectra. Use '/confirmphone ${code}' to verify this phone number or '/rejectphone ${code}' to cancel this.`,
		};
		for (const i of car) {
			message.to = i.replace("%s", phone);
			transporter.sendMail(message, (err) => {
				if (err) console.log(err);
			})
		}
	}

	_vemail(code) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE pendingemail=${mysql.escape(code)}`, (err, rows) => {
				console.log(rows);
				if (err || !rows || !rows[0]) {
					resolve({status: 0, reply: err || "No User"});
					return
				}
				let ver = rows[0].verified || 0;
				let email = rows[0].email || null;
				let userid = rows[0].userid || null;
				let pe = rows[0].pendingemail || null;
				if (pe && (ver === 1 || ver === 3 || ver === 8 || ver === 10 || ver === 12)) {
					if (pe === code) {
						//Validation successful
						switch(ver) {
						case 1:
						case 10: ver = 4; break;
						case 3:
						case 12: ver = 7; break;
						case 8: ver = 6; break;
						}

						this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(ver)} WHERE pendingemail=${mysql.escape(code)}`, async (err) => {
							if (err) {
								console.log(err);
								resolve({status: 0, reply: "Login Server Error"});
							} else {
								let type = await this.userType(userid);
								this.send([
									this.conf.sim.host,
									this.conf.sim.sPort
								], {
									act: "everif",
									userid: userid,
									type: type
								});
								resolve({status: 1, reply: "Success"});
							}
						});
					} else {
						resolve({status: 0, reply: "Invalid Code"});
					}
				} else {
					resolve({status: 0, reply: "Not Pending Email Verification"});
				}
			});
		}.bind(this));
	}

	_xemail(code) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE pendingemail=${mysql.escape(code)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve({status: 0, reply: err || "No User"});
					return
				}
				let ver = rows[0].verified || 0;
				let pe = rows[0].pendingemail || null;
				if (pe && (ver === 10 || ver === 12 || ver === 6 || ver === 8 || ver === 7 || ver === 1 || ver === 4 || ver === 3)) {
					if (pe === code) {
						//Validation successful
						switch(ver) {
						case 10: ver = 9; break;
						case 12: ver = 11; break;
						case 6:
						case 8: ver = 5; break;
						case 7: ver = 2; break;
						case 1:
						case 4: ver = 0; break; 
						case 3: ver = 2; break;
						}

						this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(ver)} AND email=NULL AND pendingemail=NULL WHERE pendingemail=${mysql.escape(code)}`, (err) => {
							if (err) {
								console.log(err);
								resolve({status: 0, reply: "Login Server Error"});
							} else {
								resolve({status: 1, reply: "Success"});
							}
						});
					} else {
						resolve({status: 0, reply: "Invalid Code"});
					}
				} else {
					resolve({status: 0, reply: "Not Pending Email Verification"});
				}
			});
		}.bind(this));
	}

	removeEmail(userid) {
		return new Promise(async function(resolve, reject) {
			let ver = await this.verif(userid);
			if (!ver) return resolve(null);

			switch(ver) {
			case 10: ver = 9; break;
			case 12: ver = 11; break;
			case 6:
			case 8: ver = 5; break;
			case 7: ver = 2; break;
			case 1:
			case 4: ver = 0; break; 
			case 3: ver = 2; break;
			}

			this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(ver)} AND email=NULL AND pendingemail=NULL WHERE pendingemail=${mysql.escape(code)}`, (err) => {
				if (err) {
					console.log(err);
					resolve(null);
				} else {
					resolve({status: 1, reply: "Success"});
				}
			});
		}.bind(this));
	}

	_vphone(code) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE pendingsms=${mysql.escape(code)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve({status: 0, reply: err || "No User"});
					return;
				}
				let ver = rows[0].verified || 0;
				let phone = rows[0].phone || null;
				let ps = rows[0].pendingphone || null;
				if (phone && ps && (ver === 2 || ver === 3 || ver === 7 || ver === 11 || ver === 12)) {
					if (pe === code) {
						//Validation successful
						switch(ver) {
						case 2:
						case 11: ver = 5; break;
						case 3:
						case 12: ver = 8; break;
						case 7: ver = 6; break;
						}

						this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(ver)} WHERE pendingsms=${code}`, (err) => {
							if (err) {
								console.log(err);
								resolve({status: 0, reply: "Login Server Error"});
							} else {
								resolve({status: 1, reply: "Success"});
							}
						});
					} else {
						resolve({status: 0, reply: "Invalid Code"});
					}
				} else {
					resolve({status: 0, reply: "Not Pending Email Verification"});
				}
			});
		}.bind(this));
	}

	_verify(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve({status: 0, reply: err || "No User"});
					return;
				}
				let ver = rows[0].verified || 0;

				if (ver < 4) {
					ver += 9;
					this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(ver)} WHERE userid=${mysql.escape(userid)}`, (err) => {
						if (err) {
							console.log(err);
							resolve({status: 0, reply: "Login Server Error"});
						} else {
							resolve({status: 1, reply: "Success"});
						}
					});
				} else {
					resolve({status: 1, reply: "Success"});
				}
			});
		}.bind(this));
	}

	_regdate(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT registertime FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) return resolve({status: 0, reply: "Unregistered"});
				resolve({status: 1, reply: rows[0].registertime});
			});
		}.bind(this));
	}

	_seen(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT lastseen FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) return resolve({status: 0, reply: "Never"});
				resolve({status: 1, reply: rows[0].lastseen});
			});
		}.bind(this));
	}

	_deregister(userid) {
		console.log("Deregister fired - userid: " + userid)
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve(err || "No User");
					return;
				}
				let ver = rows[0].verified || 0;

				const tasks = [];

				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`DELETE FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err) => {
						resolve(err || null);
					});
				}));

				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`DELETE FROM ${this.dbSessions} WHERE userid=${mysql.escape(userid)}`, (err) => {
						resolve(err || null);
					});
				}));

				Promise.all(tasks).then((errs) => {
					for (const i of errs) {
						if (i) resolve(i);
					}
					resolve(null);
				});
			});
		}.bind(this));
	}

	_disableuserid(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				
				const tasks = [];
				if (!err && rows && rows[0]) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`DELETE FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err) => {
							resolve(err || null);
						});
					}));

					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`DELETE FROM ${this.dbSessions} WHERE userid=${mysql.escape(userid)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				tasks.push(new Promise((resolve, reject) => {
					let o;
					try {
						this.conf.banned.push(userid);
						require("fs").writeFileSync("conf/banned.csv", this.conf.banned.join(","));
						o = null;
					} catch (e) {
						o = e;
					}
					resolve(o);
				}));

				Promise.all(tasks).then((errs) => {
					for (const i of errs) {
						if (i) resolve(i);
					}
					resolve(null);
				});
			});
		}.bind(this));
	}

	_ban(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve(err || "No User");
					return;
				}
				let email = rows[0].email || null;
				let phone = rows[0].phone || null;
				let ip = rows[0].ip || null;
				let ver = rows[0].verified || 0;

				const tasks = [];

				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=1 AND auth=" " WHERE userid=${mysql.escape(userid)}`, (err) => {
						resolve(err || null);
					});
				}));

				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`DELETE FROM ${this.dbSessions} WHERE userid=${mysql.escape(userid)}`, (err) => {
						resolve(err || null);
					});
				}));

				if (email && (ver === 4 || ver === 6 || ver === 7)) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=1 AND auth=" " WHERE email=${mysql.escape(email)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				if (phone && (ver === 5 || ver === 6 || ver === 8)) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=1 AND auth=" " WHERE phone=${mysql.escape(phone)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				if (ip) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=1 AND auth=" " WHERE ip=${mysql.escape(ip)}`, (err) => {
							resolve(err || null);
						});
					}));

					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`DELETE FROM ${this.dbSessions} WHERE ip=${mysql.escape(ip)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				Promise.all(tasks).then((errs) => {
					for (const i of errs) {
						if (i) resolve(i);
					}
					resolve(null);
				});
			});
		}.bind(this));
	}

	_unban(userid) {
		return new Promise(async function (resolve, reject) {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err || !rows || !rows[0]) {
					resolve(err || "No User");
					return;
				}
				let email = rows[0].email || null;
				let phone = rows[0].phone || null;
				let ip = rows[0].ip || null;
				let ver = rows[0].verified || 0;

				const tasks = [];

				tasks.push(new Promise((resolve, reject) => {
					this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=0 WHERE userid=${mysql.escape(userid)}`, (err) => {
						resolve(err || null);
					});
				}));

				if (email && (ver === 4 || ver === 6 || ver === 7)) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=0 WHERE email=${mysql.escape(email)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				if (phone && (ver === 5 || ver === 6 || ver === 8)) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=0 WHERE phone=${mysql.escape(phone)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				if (ip) {
					tasks.push(new Promise((resolve, reject) => {
						this.queryUsers(`UPDATE ${this.dbUsers} SET banstate=0 WHERE ip=${mysql.escape(ip)}`, (err) => {
							resolve(err || null);
						});
					}));
				}

				Promise.all(tasks).then((errs) => {
					for (const i of errs) {
						if (i) resolve(i);
					}
					resolve(null);
				});
			});
		}.bind(this));
	}

	checkBan(column, value) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE ${column}=${mysql.escape(value)}`, (err, rows) => {
				if (!err && rows && rows[0]) {
					resolve(rows[0].banstate);
				} else {
					if (err) console.log(err);
					resolve(0);
				}
			});
		});
	}

	addCode(userid, type, code) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`UPDATE ${this.dbUsers} SET ${type === "email" ? "pendingemail" : "pendingsms"}=${mysql.escape(code)} WHERE userid=${mysql.escape(userid)}`, (err) => {
				if (err) {
					console.log(err);
					return resolve(null);
				} else {
					return resolve(true);
				}
			});
		});
	}

	addUser(userid, username, password, verif, ip, email, phone, pe, ps) {
		return new Promise((resolve, reject) => {
			this.hashPass(password, (err, hash) => {
				if (err) {
					console.log(err);
					return resolve("Error Hashing Password");
				} else {
					console.log(hash);
				}
				const now = Date.now();


				//Generate SQL String
				let str = `INSERT INTO ${this.dbUsers} (userid, username, passwordhash`;
				if (email && pe) str += ", email";
				if (phone && ps) str += ", phone";
				str += ", verified";
				if (email && pe) str += ", pendingemail";
				if (phone && ps) str += ", pendingsms";
				str += ", registertime, ip) VALUES (";
				str += `${mysql.escape(userid)}, ${mysql.escape(username)}, ${mysql.escape(hash)}`;
				if (email && pe) str += `, ${mysql.escape(email)}`;
				if (phone && ps) str += `, ${mysql.escape(phone)}`;
				str += `, ${mysql.escape(verif)}`;
				if (email && pe) str += `, ${mysql.escape(pe)}`;
				if (phone && ps) str += `, ${mysql.escape(ps)}`;
				str += `, ${mysql.escape(now)}, ${mysql.escape(ip)})`;

				console.log(str);

				this.queryUsers(str, (err) => {
					if (err) {
						console.log(err);
						return resolve("Error Adding User to Database");
					}
					return resolve(false);
				});
			});
		});
	}

	hashPass(raw, callback) {
		bcrypt.genSalt(this.conf.saltRounds, (err, salt) => {
			if (err) return callback(err, null);
			bcrypt.hash(raw, salt, (err, hash) => {
				if (err) return callback(err, null);
				return callback(null, hash);
			});
		});
	}

	getHash(userid) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT passwordhash FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, results) => {
				if (err) {
					console.log(err);
					return resolve(null);
				}
				if (!results || !results[0] || results[1]) {
					console.log("No Pass");
					return resolve(null);
				}
				resolve(results[0].passwordhash);
			});
		});
	}

	tryPass(password, hash) {
		return new Promise((resolve, reject) => {
			bcrypt.compare(password, hash.toString(), (err, match) => {
				if (!err && match) {
					resolve(true);
				} else {
					if (err) console.log(err);
					resolve(false);
				}
			});
		});
	}

	loginCookie(username, sid) {
		let d = new Date();
		d.setTime(d.getTime() + (7*24*60*60*1000));
		return "username=" + username + ";sid=" + sid + ";expires=" + d.toUTCString() + ";";
	}

	createSession(sid, userid, now, ip) {
		const timeout = now + (1000 * 60 * 60 * 24 * 7);
		return new Promise((resolve, reject) => {
			this.queryUsers(`INSERT INTO ${this.dbSessions} (userid, sid, time, timeout, ip) VALUES (${mysql.escape(userid)}, ${mysql.escape(sid)}, ${mysql.escape(now)}, ${mysql.escape(timeout)}, ${mysql.escape(ip)})`, (err) => {
				if (err) return resolve(err);
				resolve(false);
			});
		});
	}

	closeSession(sid, userid) {
		let ip = null;
		return new Promise((resolve, reject) => {
			this.queryUsers(`UPDATE ${this.dbUsers} SET lastseen=${Date.now()} WHERE userid=${mysql.escape(userid)}`, (err) => {
				if (err) {
					console.log(err);
					return resolve(false);
				}
				this.queryUsers(`SELECT ip FROM ${this.dbSessions} WHERE sid=${mysql.escape(sid)} AND userid=${mysql.escape(userid)}`, (err, rows) => {
					if (err) {
						console.log(err);
						return resolve(false);
					}
					if (rows && rows[0]) {
						ip = rows[0].ip;
					}
					this.queryUsers(`DELETE FROM ${this.dbSessions} WHERE sid=${mysql.escape(sid)} AND userid=${mysql.escape(userid)}`, (err) => {
						if (err) {
							console.log(err);
							return resolve(false);
						}
						resolve(ip);
					});
				});
			});
		})
	}

	checkSession(sid, userid, ip) {
		return new Promise((resolve, reject) => {
			this.queryUsers(`SELECT * FROM ${this.dbSessions} WHERE sid=${mysql.escape(sid)} AND userid=${mysql.escape(userid)} AND ip=${mysql.escape(ip)}`, (err, rows) => {
				if (!err && rows && rows[0]) {
					//Handle Session expiry later
					resolve(true);
				} else {
					if (err) console.log(err);
					resolve(false);
				}
			});
		});
	}

	serverVerify(data) {
		return new Promise(async function(resolve, reject) {
			const {userid, userip, sid} = data;
			this.queryUsers(`SELECT * FROM ${this.dbSessions} WHERE userid=${mysql.escape(userid)} AND ip=${mysql.escape(userip)} AND sid=${mysql.escape(sid)}`, async function(err, rows) {
				if (!err && rows && rows[0]) {
					let type = await this.userType(userid);
					let auth = await this.auth(userid);
					resolve({status: 1, type: type, auth: auth});
				} else {
					if (err) console.log(err);
					resolve({status: 0, type: null});
				}
			}.bind(this));
		}.bind(this));
	}

	userData(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");
			let type = await this.userType(userid);
			let auth = await this.auth(userid);

			if (!type) type = "1";
			if (!auth) auth = " ";
			resolve({status: 1, type: type, auth: auth});
		}.bind(this));
	}

	userType(userid) {
		return new Promise(async function(resolve, reject) {
			let type = "1"; //Unregd
			this.queryUsers(`SELECT * FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				let user = null;
				if (!err && rows && rows[0]) {
					type = "2"; //registered
					user = rows[0];
				}
				if (err) console.log(err);
				if (user) {
					if (user.verified && parseInt(user.verified) >= 4) type = "4"; //verified
					if (user.banstate && parseInt(user.banstate) !== 0) type = "5"; //banned
					if (this.conf.sysops.indexOf(userid) > -1) type = "3"; //sysop
				}
				resolve(type);
			});
		}.bind(this));
	}

	verif(userid) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`SELECT verified FROM ${this.dbUsers} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (!err && rows && rows[0] && rows[0].verified && !isNaN(parseInt(rows[0].verified))) {
					resolve(parseInt(rows[0].verified));
				} else {
					if (err) console.log(err);
					resolve(null);
				}
			});
		}.bind(this));
	}

	setVerif(userid, verif) {
		return new Promise(async function(resolve, reject) {
			this.queryUsers(`UPDATE ${this.dbUsers} SET verified=${mysql.escape(verif)} WHERE userid=${mysql.escape(userid)}`, (err, rows) => {
				if (err) {
					console.log(err);
					resolve(null);
				} else {
					resolve(true);
				}
			});
		}.bind(this));
	}
};
