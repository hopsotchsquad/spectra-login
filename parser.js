"use strict";

module.exports = Base => class extends Base {
	constructor() {
		super();
		//TODO Utilize Challstr

		//Validation statuses:
		//0 = Registered - pending no validation
		//1 = Registered - pending email Validation
		//2 = Registered - pending phone validation
		//3 = Registered - pending both Email and Phone Validation
		//4 = Registered - Validated Email
		//5 = Registered - Validated Phone
		//6 = Registered - Validated Email and Phone
		//7 = Registered - Validated Email pending phone
		//8 = Registered - Validated Phone pending Email
		//9 = Registered - Manually Verified
		//10 = Registered - Manually Verified pending Email
		//11 = Registered - Manually Verified pending Phone
		//12 = Registered - Manually Verified pending Email and Phone

		//BanStates:
		//0 = unbanned
		//1 = permaban
	}

	//**********************
	//Client Request Methods
	//**********************

	receive(data) {
		//console.log(data);
		return new Promise(async function(resolve, reject) {
			const {act} = data;
			let reply = null;

			switch(act) {
			case "register": reply = await this.register(data); break;
			case "login": reply = await this.login(data); break;
			case "check": reply = await this.check(data); break;
			case "logout": reply = await this.logout(data); break;
			case "semail": reply = await this.semail(data); break; //Set Email
			case "vemail": reply = await this.vemail(data); break; //Verify Email
			case "cemail": reply = await this.cemail(data); break; //Change Email
			case "xemail": reply = await this.xemail(data); break; //Delete Email
			//case "vphone": reply = await this.vphone(data); break;
			//Awaiting support
			//case "cphone": reply = await this.changePhone(data); break;
			case "recoverpass": reply = await this.recoverPass(data); break;
			}

			resolve(reply || {status: 0, reply: "Login Server Error"});
		}.bind(this));
	}

	register(data) {
		return new Promise(async function(resolve, reject) {

			//Variables
			const username = data.username || null;
			const userid = username ? toId(username) : null;
			const password = data.password && data.cpassword && data.password === data.cpassword ? data.password : null;
			const sid = data.sid || null;
			const email = data.email || null;
			const phone = data.phone || null;
			const ip = data.ip || null;
			const challstr = data.challstr || null;
			let verif = 0;

			//Catches
			if (!username) return reject("No Username");
			if (!userid) return reject("No Userid");
			if (!password) return reject("Passwords do not match");
			//if (!captcha) reject("No Captcha Provided");
			if (!sid) return reject("No SID");
			if (!ip) return reject("No ip Provided");
			if (!challstr) return reject("No Challstr");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			//Run some tests to make sure this is valid
			let test;

			//Check for IP ban
			test = await this.checkBan("ip", ip);
			if (test === 1) return resolve({status: 0, reply: `${ip} is permanently banned`});

			//Make sure this account isn't already registered
			test = await this.isRegistered(userid);
			if (test) return resolve({status: 0, reply: `${userid} is already Registered`});

			//Make sure this account can be registered
			test = this.conf.banned.indexOf(userid) > -1 ? true :  false;
			if (test) return resolve({status: 0, reply: `${username} cannot be registered`});

			//Check for Email
			let emailcode = null;
			if (email && email !== "null") {
				if (!this.validateEmail(email)) return resolve({status: 0, reply: `${email} has an invalid host`});

				//Check for a ban
				test = await this.checkBan("email", email);
				if (test === 1) return resolve({status: 0, reply: `${email} is permanently banned`});

				//See if this email can be linked
				test = await this.canLink("email", email);
				if (!test) return resolve({status: 0, reply: `${email} cannot be linked to any more accounts`});

				//Should be a valid email, send them a verification email
				emailcode = this.genCode(32);
				this.verificationEmail(username, email, emailcode);
				verif = 1;
			}

			//Check for Phone
			let phonecode = null;
			/*
			if (phone) {
				if (!this.validatePhone(phone)) resolve({status: 0, reply: `${phone} is an invalid phone number`});

				//Check for a ban
				test = await this.checkBan("phone", phone);
				if (test === 1) resolve({status: 0, reply: `${phone} is permanently banned`});

				//See if this phone can be linked
				test = await this.canLink("phone", phone);
				if (!test) resolve({status: 0, reply: `${phone} cannot be linked to any more accounts`});

				//Should be a valid phone number, send them a verification sms
				phonecode = this.genCode(6);
				this.verificationSMS(username, phone, phonecode);
				verif += 2;
			}
			*/

			const regErr = await this.addUser(userid, username, password, verif, ip, email, phone, emailcode, phonecode);
			if (regErr) {
				return reject(regErr);
			} else {
				this.send([
					this.conf.sim.host,
					this.conf.sim.sPort
				], {
					act: "reg",
					challstr: challstr,
					ip: ip
				});
				resolve({status: 1, reply: "Success"});
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	login(data) {
		return new Promise(async function(resolve, reject) {

			//Variables
			const username = data.username || null;
			const userid = username ? toId(username) : null;
			const password = data.password || null;
			let sid = data.sid && data.sid !== 'null' ? data.sid : null;
			const ip = data.ip || null;
			const challstr = data.challstr || null;

			//Catches
			if (!username) return reject("No Username");
			if (!userid) return reject("No Userid");
			if (!ip) return reject("No ip Provided");
			if (!challstr) return reject("No Challstr");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			//Check for IP ban
			let banned = await this.checkBan("ip", ip);
			if (banned === 1) return resolve({status: 0, reply: `${ip} is permanently banned`});

			//Make sure this account can be registered
			let disabled = this.conf.banned.indexOf(userid) > -1 ? true :  false;
			if (disabled) return resolve({status: 0, reply: `${username} is disabled`});

			//Check for userid ban
			banned = await this.checkBan("userid", userid);
			if (banned === 1) return resolve({status: 0, reply: `${username} is permanently banned`});

			//Check to see if the userid is registered

			let regd = await this.isRegistered(userid);
			if (regd) {

				//Try password
				if (!password) {
					resolve({status: 0, reply: "passwordprompt"});
					return;
				} else {
					const hash = await this.getHash(userid);
					if (!hash) return reject("Unable to get Password Hash");
					const pass = await this.tryPass(password, hash);
					if (!pass) return resolve({status: 0, reply: "Incorrect Password"});
				}
			}

			//Deal with sid
			if (sid) {
				this.closeSession(sid, userid); //close old session
			} else {
				sid = this.genSID(); //make a sid
			}

			const fail = await this.createSession(sid, userid, Date.now(), ip);
			if (fail) {
				reject(fail);
			} else {
				const auth = await this.auth(userid);
				this.send([
					this.conf.sim.host,
					this.conf.sim.sPort
				], {
					act: "login",
					challstr: challstr,
					sid: sid,
					username: username,
					ip: ip,
					auth: auth
				});
				resolve({status: 1, reply: sid});
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	check(data) {
		return new Promise(async function(resolve, reject) {
			const username = data.username || null;
			const userid = toId(username);
			const sid = data.sid || null;
			const ip = data.ip || null;
			const challstr = data.challstr || null;

			if (!ip) return reject("No IP");
			if (!challstr) return reject("No Challstr");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			//Check for IP ban
			let banned = await this.checkBan("ip", ip);
			if (banned === 1) return resolve({status: 0, reply: `${ip} is permanently banned`});

			//Make sure this account can be registered
			let disabled = this.conf.banned.indexOf(userid) > -1 ? true :  false;
			if (disabled) return resolve({status: 0, reply: `${username} is disabled`});

			//Check for userid ban
			banned = await this.checkBan("userid", userid);
			if (banned === 1) return resolve({status: 0, reply: `${username} is permanently banned`});

			//Verify Session
			const valid = await this.checkSession(sid, userid, ip);
			if (!valid) {
				resolve({status: 0, reply: "Invalid Session"});
			} else {
				const auth = await this.auth(userid);
				this.send([
					this.conf.sim.host,
					this.conf.sim.sPort
				], {
					act: "login",
					challstr: challstr,
					sid: sid,
					username: username,
					ip: ip,
					auth: auth
				});
				resolve({status: 1, reply: "Success"});
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	logout(data) {
		return new Promise(async function(resolve, reject) {

			//Variables
			const username = data.username || null;
			const userid = toId(username) || null;
			const sid = data.sid || null;
			const ip = data.ip || null;
			const challstr = data.challstr || null;

			//Catches
			if (!username) return reject("No Username");
			if (!userid) return reject("No Userid");
			if (!sid) return reject("No sid");
			if (!ip) return reject("No ip Provided");
			if (!challstr) return reject("No Challstr");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			const pass = await this.closeSession(sid, userid);
			if (!pass) return reject("Error Closing Session");
			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	semail(data) {
		return new Promise(async function(resolve, reject) {
			let {userid, password, email, sid, challstr, ip} = data;

			if (!userid) return reject("No userid");
			if (!password) return reject("No Password");
			if (!email) return reject("No Email");
			if (!sid) return reject("No SID");
			if (!challstr) return reject("No Challstr");
			if (!ip) return reject("No IP");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			//Verify Session
			const valid = await this.checkSession(sid, userid, ip);
			if (!valid) {
				resolve({status: 0, reply: "Invalid Session"});
			} else {
				
				let regd = await this.isRegistered(userid);
				if (!regd) return resolve({status: 0, reply: "Unregistered"});
				const hash = await this.getHash(userid);
				if (!hash) return reject("Unable to get Password Hash");
				const pass = await this.tryPass(password, hash);
				if (!pass) return resolve({status: 0, reply: "Incorrect Password"});

				if (!this.validateEmail(email)) return resolve({status: 0, reply: `${email} has an invalid host`});
				
				//Check for a ban
				let test = await this.checkBan("email", email);
				if (test === 1) return resolve({status: 0, reply: `${email} is permanently banned`});

				//See if this email can be linked
				test = await this.canLink("email", email);
				if (!test) return resolve({status: 0, reply: `${email} cannot be linked to any more accounts`});

				//Should be a valid email, send them a verification email
				let emailcode = this.genCode(32);
				this.verificationEmail(userid, email, emailcode);

				let sta = await this.addCode(userid, "email", emailcode);
				if (!sta) return reject("Unable to set verif code")

				let verif = await this.verif(userid);
				if (!verif) return reject("No Verif");

				switch(verif) {
				case 0: verif = 1; break;
				case 2: verif = 3; break;
				case 6:
				case 5: verif = 8; break;
				case 11:
				case 7: verif = 12; break;
				case 4:
				case 9: verif = 10; break;
				}
				verif = await this.setVerif(userid, verif);
				if (!verif) return reject("Unable to update verification status");
				resolve({status: 1, reply: "Success"});
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		})
	}

	vemail(data) {
		return new Promise(async function(resolve, reject) {
			console.log("Verify Email fired");
			const code = data.code || null;
			console.log("Code: " + code);
			if (!code) return reject("No Code");
			const reply = await this._vemail(code);
			if (!reply) return reject("_vemail err");
			resolve(reply);
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	cemail(data) {
		return new Promise(async function(resolve, reject) {
			const userid = data.userid || null;
			const challstr = data.challstr || null;
			const sid = data.sid || null;
			const ip = data.ip || null;

			if (!userid) return reject("No Userid");
			if (!challstr) return reject("No challstr");
			if (!sid) return reject("No SID");
			if (!ip) return reject("No IP");

			//Verify Challstr
			const chall = await this.challenge(challstr, ip);
			if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

			//Verify Session
			const valid = await this.checkSession(sid, userid, ip);
			if (!valid) return reject("Invalid Session");

			//Run Operation
			const reply = await this._cemail(userid);
			if (!reply) return reject("_cemail err");

			this.send([
				this.conf.sim.host,
				this.conf.sim.sPort
			], {
				act: "cemail",
				userid: userid
			});
			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	xemail(data) {
		return new Promise(async function(resolve, reject) {
			const code = data.code || null;
			const userid = data.userid || null;
			const challstr = data.challstr || null;
			const sid = data.sid || null;
			const ip = data.ip || null;

			if (!code && (!userid || !challstr || !sid || !ip)) return reject("No Code");
			if (!code && !userid) return reject("No Userid");
			if (!code && !challstr) return reject("No challstr");
			if (!code && !sid) return reject("No SID");
			if (!code && !ip) return reject("No IP");

			if (code) {
				//Non-Client
				const reply = await this._xemail(code);
				if (!reply) return reject("_xemail err");
				resolve(reply);
			} else {
				//Client

				//Verify Challstr
				const chall = await this.challenge(challstr, ip);
				if (!chall || chall.fail || !chall.pass) return reject("Invalid Challstr");

				//Verify Session
				const valid = await this.checkSession(sid, userid, ip);
				if (!valid) return reject("Invalid Session");

				//Run Operation
				const creply = await this.removeEmail(userid);
				if (!creply) return reject("_removeEmail err");

				const ver = await this.verif(userid);
				this.send([
					this.conf.sim.host,
					this.conf.sim.sPort
				], {
					act: "removeemail",
					userid: userid,
					ver: ver
				});
				resolve(creply);
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	vphone(data) {
		return new Promise(async function(resolve, reject) {
			const code = data.code || null;
			if (!code) return reject("No Code");
			if (!sid) return reject("No sid");
			const reply = await this._vphone(code);
			if (!reply) return reject("_vphone err");
			resolve(reply);
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	//***********************
	// Server Request Methods
	//***********************

	_receive(data) {
		return new Promise(async function(resolve, reject) {
			const {act, ip} = data;
			if (ip !== this.conf.sim.ip) reject("Unexpected Request Source");
			let reply = null;

			switch(act) {
			case "verify": reply = await this.serverVerify(data); break;
			case "login": reply = await this._login(data); break;
			case "isreg": reply = await this.isreg(data); break;
			case "setauth": reply = await this.setAuth(data); break;
			case "getauth": reply = await this.getAuth(data); break;
			case "getemail": reply = await this.getEmail(data); break;
			case "regdate": reply = await this.regdate(data); break;
			case "seen": reply = await this.seen(data); break;
			case "ban": reply = await this.ban(data); break;
			case "unban": reply = await this.unban(data); break;
			case "deregister": reply = await this.deregister(data); break;
			case "disableuserid": reply = await this.disableuserid(data); break;
			case "health": reply = await this.health(data); break;
			case "eval": reply = await this.eval(data); break;
			case "sql": reply = await this.sql(data); break;
			case "verifyuser": reply = await this.manualVerify(data); break;
			case "userdata": reply = await this.userData(data); break;
			}

			resolve(reply || {status: 0, reply: "Login Server Error"});
		}.bind(this));
	}

	_login(data) {
		return new Promise(async function(resolve, reject) {

			//Used by the server to login to an unregistered alt
			//With an ongoing session
			const {username, curuser, sid, userip} = data;

			if (!username) return reject("No Username");
			const userid = toId(username);
			if (!curuser) return reject("No curuser")
			if (!sid) return reject("No SID");
			if (!userip) return reject("No IP");

			//Make sure this account can be registered
			let disabled = this.conf.banned.indexOf(userid) > -1 ? true : false;
			if (disabled) return resolve({status: 0, reply: `disabled`});

			//Check for userid ban
			let banned = await this.checkBan("userid", userid);
			if (banned === 1) return resolve({status: 0, reply: `banned`});

			let regd = await this.isRegistered(userid);
			if (regd) return resolve({status: 0, reply: "registered"});

			let newip = await this.closeSession(sid, curuser); //close old session

			const fail = await this.createSession(sid, toId(username), Date.now(), userip);
			if (fail) {
				reject(fail);
			} else {
				resolve({status: 1, reply: "success"});
			}
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	isreg(data) {
		return new Promise(async function(resolve, reject) {
			//Variables
			let userid = data.userid || null;
			if (!userid) return reject("No Userid");

			const reply = await this.isRegistered(userid);

			if (reply) {
				resolve({status: 1, reply: "Registered"});
			} else {
				resolve({status: 1, reply: "Unregistered"});
			}
		}.bind(this));
	}

	setAuth(data) {
		return new Promise(async function(resolve, reject) {
			//Variables
			const {userid, auth} = data;

			if (!userid) return reject("No userid");
			if (!auth) return reject("No Target Auth");

			const regd = await this.isRegistered(userid);
			if (!regd) return resolve({status: 0, reply: `${userid} is not registered`});

			const fail = await this._setAuth(userid, auth);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	getAuth(data) {
		return new Promise(async function(resolve, reject) {
			const auth = await this._getAuth();
			return resolve(auth || {status: 0, reply: "Login Server Error"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	getEmail(data) {
		return new Promise(async function(resolve, reject) {
			const userid = data.userid || null;
			if (!userid) return reject("No Userid");
			const email = await this._getEmail(userid);
			if (email) return resolve({status: 1, reply: email});
			return resolve({status: 0, reply: "Login Server Error"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	ban(data) {
		return new Promise(async function(resolve, reject) {

			const {userid} = data;
			if (!userid) return reject("No userid");

			const fail = await this._ban(userid);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	unban(data) {
		return new Promise(async function(resolve, reject) {

			const {userid} = data;
			if (!userid) return reject("No userid");

			const fail = await this._unban(userid);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	regdate(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");

			const r = await this._regdate(userid);
			resolve(r);
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	seen(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");

			const r = await this._seen(userid);
			resolve(r);
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	manualVerify(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");

			const fail = await this._verify(userid);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	deregister(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");

			const fail = await this._deregister(userid);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	disableuserid(data) {
		return new Promise(async function(resolve, reject) {
			const {userid} = data;
			if (!userid) return reject("No Userid");

			const fail = await this._disableuserid(targetUserid);
			if (fail) return reject(fail);

			resolve({status: 1, reply: "Success"});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	health(data) {
		return new Promise(async function(resolve, reject) {
			let resources;
			try {
				let date = Date.now();
				let memUsage = process.memoryUsage();
				let results = [memUsage.rss, memUsage.heapUsed, memUsage.heapTotal];
				let units = ["B", "KB", "MB", "GB", "TB"];
				for (let i = 0; i < 3; i++) {
					let unitIndex = Math.floor(Math.log2(results[i]) / 10);
					results[i] = "" + (results[i] / Math.pow(2, 10 * unitIndex)).toFixed(2) + " " + units[unitIndex];
				}
				resources = `Resource Usage: RSS: ${results[0]} - Heap: ${results[1]} / ${results[2]}`;
			} catch (e) {
				resources = 'Resource Usage: Unavailable';
			}

			//TODO Add Usage, Session, and Emailer Health
			//const reply = [resources];
			//reply = reply.join("\n");
			resolve({status: 1, reply: resources});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	eval(data) {
		return new Promise(async function(resolve, reject) {
			let c = data.code ? data.code : "";
			resolve({status: 1, reply: (function(str) {
				try {
					str = eval(str);
				} catch(e) {
					str = (e.stack).toString();
				}
				return str;
			}.call(ctx, c))});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	sql(data) {
		return new Promise(async function(resolve, reject) {
			const {code} = data;
			if (!code) return reject("No Code");
			this.queryUsers(code, (err,rows) => {
				let reply = [];
				if (rows && rows[0]) {
					for (let i = 0; i < (rows.length <= 5 ? rows.length : 5); i++) {
						reply.push(JSON.stringify(rows[i]));
					}
				}

				if (reply.length === 0) {
					reply = null;
				} else {
					reply = reply.join('\n');
				}
				resolve({status: 1, reply: reply, err: err})
			});
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}
}