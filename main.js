"use strict";

const fs = require('fs');
const https = require('https');
const CP = require("child_process");
global.ctx = this; //For Eval - Bad Practice, I know

global.toId = function(str) {
	return ('' + str).toLowerCase().replace(/[^a-z0-9]+/g, '');
}

const Mixin1 = require("./methods");
const Mixin2 = require("./parser");
const Mixin3 = require("./config");
class Core {};

class Methods extends Mixin1(Mixin2(Mixin3(Core))) {
	constructor() {
		super();
		this.dbInit(); //Initialize DB
	}
}

class ProcessWrapper {
	constructor(id, subProcess, status, up) {
		this.id = id;
		this.process = subProcess;
		this.status = status;
		this.up = up;

		this.process.on("message", async function(task) {
			task.data = await this.up(task.data);
			this.process.send(task);
		}.bind(this));
	}
}

class MainC extends Methods {
	constructor() {
		super();
		this.status = "Open";
		this.processes = new Map();
	}

	init() {

		//Add Standard Worker(s)
		while(this.processes.size < (typeof this.conf.port === "object" ? this.conf.port.length : 1)) {
			this.processes.set(this.processes.size, new ProcessWrapper(
				this.processes.size, //id
				CP.fork("./worker", [this.conf.port], {cwd: __dirname}), //process
				0, //status
				this.receive.bind(this) //receive method
			));
		}

		//Add Simserver Worker
		this.processes.set(this.processes.size, new ProcessWrapper(
			this.processes.size, //id
			CP.fork("./worker", [this.conf.sim.lPort], {cws: __dirname}), //Process
			0, //status
			this._receive.bind(this) //receive method
		));
	}
}

const Main = new MainC();

Main.init();