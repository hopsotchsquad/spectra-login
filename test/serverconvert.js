'use strict';

//Server SetAuth

exports.start = async (Methods) => {
	let data, results;
	console.log("Converting Login Server");
	data = "finny=~|six= |terai= |cloyy= |vgcvgc= |spectrabot= |c733937123= |attribute= |botimusprime= |princehaunt= |jd=~|thecameldude=%|toosweet=@|cryptis=@|tailz=&|stellarbuck=@|seo=%|gyarados= |sindra= |luka=&|wgc=@|green=%|groove= |crow= |pack=%|imudz= |kip=@|thedazzlerjoe=&|ludicolo=%|lighthaze= |isawa=&|isackoshet= |feral= |candelabrum=@|paradox= |snorlax= |ttdango= |asdf= |peter= |william=%|spunik= |kswiss=%|espeon= |showdownhelper=~|nidokyng= |techworld= |mr6sixty6= |grimjou= |thedarkestlight= |jaydos= |resistanceluke= |tranquil= |deacon= |lights=~|vagabong= |unity= |megas4ever= |easye= |zetto= |crash= |vivilex= |rock= |sunnycastform= |fer= |possibilities=@|epic= |drix= |gay= |erika= |yveltal= |theinverse= |partyover= |red= |erica= |silvy= |taide= |zorquax= |roserade= |deluxe= |earthgasm= |zeno=&|deleta= |messiah= |iarkoroz= |primeval= |fun= |aszi= |meloetta= |gryph= |dialga= ";
	const m = new Map();
	data = data.split("|");
	for (const i of data) {
		results = i.split("=");
		m.set(results[0], results[1]);
	}

	const tasks = [];

	for (const [user, auth] of m) {
		tasks.push(new Promise((resolve, reject) => {
			Methods.queryUsers(`SELECT * FROM ntbb_users WHERE userid=${mysql.escape(user)}`, (err, results) => {
				if (err || !results || !results[0]) {
					resolve(err || "No User");
					return;
				}
				const {userid, username, passwordhash, ip} = results[0];
				Methods.queryUsers(`INSERT INTO users (userid, username, passwordhash, verified, registertime, ip, auth) VALUES (${mysql.escape(userid)}, ${mysql.escape(username)}, ${mysql.escape(passwordhash)}, 9, ${Date.now()}, ${mysql.escape(ip)}, ${mysql.escape(auth)})`, (err) => {
					if (err) resolve(err);
					resolve(null);
				});
			});
		}));
	}
	
	Promise.all(tasks).then((errs) => {
		let e = null;
		for (const i of errs) {
			if (i) {
				e = true;
				console.log(i);
			}
		}
		if (!e) {
			console.log("Server conversion completed without error.");
		} else {
			console.log("Server conversion completed with errors.")
		}
	});
};