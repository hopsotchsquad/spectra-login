'use strict';

//Client Logout

exports.start = async (Methods) => {
	let data, results;
	console.log("Test 1: Logout");
	data = {
		act: 'logout',
		username: 'Lights',
		sid: 'yfmMJCWj0ayMheTzBnhWQ+zA',
		ip: '174.59.24.157',
		challstr: 'null'
	};
	results = await Methods.receive(data);
	console.log(results);
};