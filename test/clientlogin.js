'use strict';

//Client Login

exports.start = async (Methods) => {
	let data, results;
	console.log("Test 1: Login - No Password - Unregistered - No SID");
	data = {
		act: 'login',
		username: 'Light',
		ip: '174.59.24.157',
		challstr: 'null'
	};
	results = await Methods.receive(data);
	console.log(results);

	console.log("Test 2: Login - Password - registered - SID");
	data = {
		act: 'login',
		username: 'Light',
		ip: '174.59.24.157',
		password: 'test',
		sid: 'yfmMJCWj0ayMheTzBnhWQ+zA',
		challstr: 'null'
	};
	results = await Methods.receive(data);
};