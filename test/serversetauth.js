'use strict';

//Server SetAuth

exports.start = async (Methods) => {
	let data, results;
	console.log("Test 1: SetAuth");
	data = {
		act: 'setauth',
		name: 'Lights',
		auth: '~'
	};
	results = await Methods._receive(data);
	console.log(results);
};