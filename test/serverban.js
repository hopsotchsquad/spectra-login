'use strict';

//Server ban

exports.start = async (Methods) => {
	let data, results;
	console.log("Test 1: ban");
	data = {
		act: 'ban',
		userid: 'lights'
	};
	results = await Methods._receive(data);
	console.log(results);

	console.log("Test 1: unban");
	data = {
		act: 'unban',
		userid: 'lights'
	};
	results = await Methods._receive(data);
	console.log(results);
};